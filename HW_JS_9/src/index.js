/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

window.addEventListener("DOMContentLoaded", function () {
    const display = document.getElementById("result");
    const buttons = document.getElementsByClassName("button");
    const equalsButton = document.getElementById("eq");

    
    let currentNumber = "";
    let previousNumber = "";
    let operator = "";
    let memory = 0;

  
    Array.from(buttons).forEach(button => {
        button.addEventListener("click", buttonClick);
    });

   
    function buttonClick(event) {
        const value = event.target.value;

        
        if (value === "*" || value === "/" || value === "+" || value === "-") {
            if (currentNumber !== "") {
                previousNumber = currentNumber;
                currentNumber = "";
                operator = value;
                equalsButton.disabled = false; 
                display.value = previousNumber + " " + operator; 
            }
        }

   
        if (value === "=") {
            if (currentNumber !== "" && previousNumber !== "") {
                const result = calculateResult(parseFloat(previousNumber), operator, parseFloat(currentNumber));
                display.value = result;
                previousNumber = "";
                currentNumber = result.toString();
                equalsButton.disabled = true; 
            }
        }

      
        if (!isNaN(value) || value === ".") {
            if (equalsButton.disabled === true) {
                currentNumber = value;
                display.value = currentNumber;
                equalsButton.disabled = false; 
            } else {
                currentNumber += value;
                display.value = currentNumber;
            }
        }


        
        if (value === "C") {
            currentNumber = "";
            display.value = "";
            equalsButton.disabled = true; 
        }

       
        if (value === "m+") {
            memory += parseFloat(display.value);
            display.value = "m";
        }

      
        if (value === "m-") {
            memory -= parseFloat(display.value);
            display.value = "m";
        }

   
        if (value === "mrc") {
            display.value = memory.toString();
            memory = 0;
        }
    }

    
    function calculateResult(num1, operator, num2) {
        let result = 0;

        switch (operator) {
            case "*":
                result = num1 * num2;
                break;
            case "/":
                result = num1 / num2;
                break;
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            default:
                break;
        }

        return result;
    }
})