const perPage = 100;
const commentsContainer = document.getElementById('comments');
const paginationContainer = document.getElementById('pagination');

function fetchComments(page = 1) {
    const perPage = 100;
    const url = `https://jsonplaceholder.typicode.com/comments?_page=${page}&_limit=${perPage}`;

    fetch(url)
        .then(response => {
            const totalComments = response.headers.get('X-Total-Count');
            const totalPages = Math.ceil(totalComments / perPage);
            renderPagination(page, totalPages);
            return response.json();
        })
        .then(comments => {
            renderComments(comments);
        })
        .catch(error => console.log(error));
}

function renderComments(comments) {
    commentsContainer.innerHTML = '';

    let commentRow;

    for (let i = 0; i < comments.length; i++) {
        if (i % 2 === 0) {
            commentRow = document.createElement('div');
            commentRow.classList.add('comment-row');
            commentsContainer.appendChild(commentRow);
        }

        const comment = comments[i];

        const card = document.createElement('div');
        card.classList.add('card', 'comment-card');

        const cardBody = document.createElement('div');
        cardBody.classList.add('card-body');

        const commentId = document.createElement('h5');
        commentId.classList.add('card-title');
        commentId.innerText = `ID: ${comment.id}`;

        const commentName = document.createElement('h6');
        commentName.classList.add('card-subtitle', 'mb-2', 'text-muted');
        commentName.innerText = `Name: ${comment.name}`;

        const commentEmail = document.createElement('p');
        commentEmail.classList.add('card-text');
        commentEmail.innerText = `Email: ${comment.email}`;

        const commentBody = document.createElement('p');
        commentBody.classList.add('card-text');
        commentBody.innerText = `Body: ${comment.body}`;

        cardBody.appendChild(commentId);
        cardBody.appendChild(commentName);
        cardBody.appendChild(commentEmail);
        cardBody.appendChild(commentBody);

        card.appendChild(cardBody);
        commentRow.appendChild(card);
    }
}

function renderPagination(currentPage, totalPages) {
    paginationContainer.innerHTML = '';

    for (let i = 1; i <= totalPages; i++) {
        const pageLink = document.createElement('a');
        pageLink.classList.add('btn', 'btn-primary', 'mr-2');
        pageLink.innerText = i;
        pageLink.addEventListener('click', () => fetchComments(i));

        if (i === currentPage) {
            pageLink.classList.add('active');
        }

        paginationContainer.appendChild(pageLink);
    }
}

fetchComments();