/*
Створіть картинку та кнопку з назвою "Змінити картинку" зробіть так щоб при завантаженні сторінки була картинка https://itproger.com/img/courses/1476977240.jpg При натисканні на кнопку вперше картинка замінилася на https://itproger.com/img/courses/1476977488.jpg при другому натисканні щоб картинка замінилася на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
Зробити робочими кнопки з виводом відповідних дій на сторінку приклад 008_DOM

*/





function swapInputs() {
  var input1 = document.getElementById("input1").value;
  var input2 = document.getElementById("input2").value;

  document.getElementById("input1").value = input2;
  document.getElementById("input2").value = input1;
}


//-----------------------------------------------------------------

function generateDiv() {
  const inputText = document.getElementById("input").value;
  const outputDiv = document.getElementById("output");
  outputDiv.innerHTML = "<div>" + inputText + "</div>";
  document.getElementById("input").value = "";
}


//-----------------------------------------------------------------

window.onload = function () {

  const task2 = document.getElementById("task2");

  for (let i = 1; i <= 5; i++) {
    const newDiv = document.createElement("div");
    task2.appendChild(newDiv);
  }

  const [...divs] = document.getElementsByTagName("div");

  divs.forEach((e) => {
    e.addEventListener("click", () => {
      e.style.backgroundColor = "red";
    })
  });


//-----------------------------------------------------------------


  const task4 = document.getElementById("task4");


  for (let i = 1; i <= 10; i++) {
    const p = document.createElement("p");
    p.innerText = `Параграф ${i}`;
    p.onclick = () => {p.remove()}
    task4.appendChild(p);

  }

}


var imgArray = [
  "https://itproger.com/img/courses/1476977488.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png"
];

var imgIndex = 0;

function changeImg() {
  var img = document.getElementById("myImg");
  imgIndex++;
  if (imgIndex >= imgArray.length) {
    imgIndex = 0;
  } 
    img.src = imgArray[imgIndex];
  
}