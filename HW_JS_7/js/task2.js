function selectFirstChild() {
  clearSelection();
  var ul = document.getElementById('list');
  var firstChild = ul.firstElementChild;

  if (firstChild && firstChild.nodeName === 'LI') {
    firstChild.style.backgroundColor = 'yellow';
  }
}

function selectLastChild() {
  clearSelection();
  var ul = document.getElementById('list');
  var lastChild = ul.lastElementChild;

  if (lastChild.nodeName === 'LI') {
    lastChild.style.backgroundColor = 'yellow';
  }
}

function selectNextNode() {
  var currentSelection = document.querySelector('#list li[style="background-color: yellow;"]');
  if (currentSelection) {
    currentSelection.style.backgroundColor = '';
    var nextSibling = currentSelection.nextElementSibling;

    while (nextSibling !== null && nextSibling.nodeName !== 'LI') {
      nextSibling = nextSibling.nextElementSibling;
    }

    if (nextSibling && nextSibling.nodeName === 'LI') {
      nextSibling.style.backgroundColor = 'yellow';
    }
  }
}

function selectPrevNode() {
  var currentSelection = document.querySelector('#list li[style="background-color: yellow;"]');
  if (currentSelection) {
    currentSelection.style.backgroundColor = '';
    var prevSibling = currentSelection.previousElementSibling;

    while (prevSibling !== null && prevSibling.nodeName !== 'LI') {
      prevSibling = prevSibling.previousElementSibling;
    }

    if (prevSibling && prevSibling.nodeName === 'LI') {
      prevSibling.style.backgroundColor = 'yellow';
    }
  }
}

function createNewChild() {
  clearSelection();
  var ul = document.getElementById('list');
  var newListItem = document.createElement('li');
  newListItem.appendChild(document.createTextNode('New List Item'));
  ul.appendChild(newListItem);
}

function removeLastChild() {
  clearSelection();
  var ul = document.getElementById('list');
  var lastChild = ul.lastElementChild;

  if (lastChild.nodeName === 'LI') {
    ul.removeChild(lastChild);
  }
}

function createNewChildAtStart() {
  clearSelection();
  var ul = document.getElementById('list');
  var newListItem = document.createElement('li');
  newListItem.appendChild(document.createTextNode('New List Item'));
  ul.insertBefore(newListItem, ul.firstElementChild);
}
  


function clearSelection() {
  var listItems = document.querySelectorAll('#list li');
  listItems.forEach(function(item) {
    item.style.backgroundColor = '';
  });
}