// https://rickandmortyapi.com/api 

async function getData(url, method = "GET") {
    const data = await fetch(url, { method });
    return data.json();
}


getData("https://rickandmortyapi.com/api")
    .then((info) => {
       
        const arr = Object.entries(info);
        if (!Array.isArray(arr)) return;
        arr.forEach((item) => {
            createCard(item)
        })
    })

function createCard(element) {
    //console.log(element);
    const [key] = element
    const card = document.createElement("div");
    card.innerText = key;
    card.className = "card";
    card.addEventListener("click", () => {
        const wind = window.open( `./page/index.html?${key}`);
    })
    document.querySelector(".cards").append(card);
}