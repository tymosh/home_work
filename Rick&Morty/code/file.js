const category = document.location.search.substr(1);
document.querySelector("title").innerHTML = category;

async function req(url) {
    const data = await fetch(url)
    return data.json()
}

if (category === "characters") {
    req("https://rickandmortyapi.com/api/character")
        .then((data) => {
            showInfo(data)
        }).catch((e) => { console.error(e) })
} else if (category === "locations") {
    req("https://rickandmortyapi.com/api/location").then((data) => {
        showInfo(data)
    }).catch((e) => { console.error(e) });
} else if (category === "episodes") {
    req("https://rickandmortyapi.com/api/episode").then((data) => {
        showInfo(data)
    }).catch((e) => { console.error(e) });
} else {
    console.error("")
}


function showInfo(e) {
    const divs = e.results.map((el) => {
        const div = document.createElement("div");
        div.className = "card"
        const pattern = `<div class="img-card">
        ${el.image ? `<img src="${el.image}" alt="${el.name}">` : el.type ? el.type : el.air_date
            }
        </div>
        <h3>${el.name}</h3>
        ${el.species ? `<p>${el.species}</p>` : el.episode ? `<p>${el.episode}</p>` : ""}
        `
        div.insertAdjacentHTML("beforeend", pattern);
        div.addEventListener("click", (e) => {
            openInfo(el, e)
        })
        return div;
    });

    document.querySelector(".cards").append(...divs)
}

async function openInfo(card, event) {
    const modal = document.querySelector(".box-modal");
    modal.classList.add("active");
    document.querySelector("#modal-closed").addEventListener("click", () => {
        modal.classList.remove("active");
    });

    const modalBody = document.querySelector(".modal-body");
    modalBody.innerHTML = '';

    let content = '';

    if (category === "characters") {
        content += `<div class="img-card">
        <img src="${card.image}" alt="${card.name}"></div>`;
        content += `<div class="info-card"><p>${card.name}</p>
        <p>${card.status} - ${card.species}</p>
        <p>${card.type} </p>
        <p>${card.location.name} </p>
        <p>${card.episode[0].name} </p>
        <p>${card.gender} </p>
        </div>`;
    }
    else if (category === "locations") {
        content += `<div class="img-card">
        <img src="${card.image}" alt="${card.name}"></div>`;
        content += `<div class="info-card"><p>${card.name}</p>
        <p>${card.status} - ${card.species}</p></div>`;
    }
    else if (category === "episodes") {
        content += `<div class="img-card">
        <img src="${card.image}" alt="${card.name}"></div>`;
        content += `<div class="info-card"><p>${card.name}</p>
        <p>${card.status} - ${card.species}</p></div>`;
    }
    else{}

        modalBody.innerHTML = content;
        modalBody.appendChild(modalBody);
}
