/*
  Створіть програму секундомір.
         
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
* Виведення лічильників у форматі ЧЧ:ММ:СС<br>
 * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції


  

*/ 
window.addEventListener("DOMContentLoaded", function() {

  const startBtn = document.getElementById("startBtn");
  const stopBtn = document.getElementById("stopBtn");
  const resetBtn = document.getElementById("resetBtn");
  const hoursDisplay = document.getElementById("hours");
  const minutesDisplay = document.getElementById("minutes");
  const secondsDisplay = document.getElementById("seconds");
  const containerStopwatch = document.querySelector(".container-stopwatch");

  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  let timerInterval;
  let isRunning = false; // New variable to track if the stopwatch is already running

  const updateTimer = () => {
    seconds++;
    if (seconds === 60) {
      seconds = 0;
      minutes++;
    }
    if (minutes === 60) {
      minutes = 0;
      hours++;
    }
    hoursDisplay.textContent = formatTime(hours);
    minutesDisplay.textContent = formatTime(minutes);
    secondsDisplay.textContent = formatTime(seconds);
  };

  const formatTime = (time) => {
    return time < 10 ? `0${time}` : time;
  };

  startBtn.addEventListener("click", () => {
    if (!isRunning) { // Check if the stopwatch is already running
      containerStopwatch.classList.remove("red", "silver");
      containerStopwatch.classList.add("green");
      timerInterval = setInterval(updateTimer, 1000);
      isRunning = true; // Update the isRunning variable
    }
  });

  stopBtn.addEventListener("click", () => {
    if (isRunning) { // Check if the stopwatch is running before stopping it
      containerStopwatch.classList.remove("green", "silver");
      containerStopwatch.classList.add("red");
      clearInterval(timerInterval);
      isRunning = false; // Update the isRunning variable
    }
  });

  resetBtn.addEventListener("click", () => {
    containerStopwatch.classList.remove("green", "red");
    containerStopwatch.classList.add("silver");
    clearInterval(timerInterval);
    hours = 0;
    minutes = 0;
    seconds = 0;
    hoursDisplay.textContent = "00";
    minutesDisplay.textContent = "00";
    secondsDisplay.textContent = "00";
    isRunning = false; // Reset the isRunning variable
  });

});
