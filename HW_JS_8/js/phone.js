/*

Реалізуйте програму перевірки телефону<br>
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження<br>
* Користувач повинен ввести номер телефону у форматі 0(Початок з 0)ХХ-ХХХ-ХХ-ХХ <br>
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер
  правильний
  зробіть зелене тло і використовуючи document.location переведіть через 3с користувача на сторінку
  https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
  якщо буде помилка, відобразіть її в діві до input.

*/ 
window.addEventListener("DOMContentLoaded" ,function(){
    function validatePhoneNumber(phoneNumber) {
        var pattern = /^0\d{2}-\d{3}-\d{2}-\d{2}$/;
        return pattern.test(phoneNumber);
      }
    
      function redirectToURL(url) {
        setTimeout(function() {
          document.location = url;
        }, 3000);
      }
    
      document.getElementById('save-button').addEventListener('click', function() {
        var phoneNumber = document.getElementById('phone-input').value.trim();
    
        if (validatePhoneNumber(phoneNumber)) {
          document.body.classList.add('success');
          redirectToURL('https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg');
        } else {
          document.getElementById('error-message').textContent = 'Неправильний формат номера телефону!';
        }
      });
})
