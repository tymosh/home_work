class Car {
    constructor(brand, carClass, weight, driver, engine) {
        this.brand = brand;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    start() {
        console.log("Поїхали");
    }

    stop() {
        console.log("Зупиняємося");
    }

    turnRight() {
        console.log("Поворот праворуч");
    }

    turnLeft() {
        console.log("Поворот ліворуч");
    }

    toString() {

        console.log(`Марка автомобіля: ${this.brand}`);
        console.log(`Клас автомобіля: ${this.carClass}`);
        console.log(`Вага: ${this.weight}`);
        console.log(`Водій: ${this.driver.name}, стаж водіння: ${this.driver.experience} років`);
        console.log(`Двигун: потужність - ${this.engine.power} к.с., виробник - ${this.engine.manufacturer}`);

        if (this.speed) {
            console.log(`Швидкість: ${this.speed}`);
        }
        else if (this.carrying) {
            console.log(`Вантажопідйомність кузова: ${this.carrying}`);
        }

    }
/*    
toString() {
        let result = '';
        result += `Марка автомобіля: ${this.brand}\n`;
        result += `Клас автомобіля: ${this.carClass}\n`;
        result += `Вага: ${this.weight}\n`;
        result += `Водій: ${this.driver.name}, стаж водіння: ${this.driver.experience} років\n`;
        result += `Двигун: потужність - ${this.engine.power} к.с., виробник - ${this.engine.manufacturer}\n`;
        if (this.speed) {
            result += `Швидкість: ${this.speed}\n`;
        } else if (this.carrying) {
            result += `Вантажопідйомність кузова: ${this.carrying}\n`;
        }
        return result;
    }
    */
}


class Driver {
    constructor(name, experience) {
        this.name = name;
        this.experience = experience;
    }
}

class Engine {
    constructor(power, manufacturer) {
        this.power = power;
        this.manufacturer = manufacturer;
    }
}

class Lorry extends Car {
    constructor(brand, carClass, weight, driver, engine, carrying) {
        super(brand, carClass, weight, driver, engine)
        this.carrying = carrying

    }
}

class SportCar extends Car {
    constructor(brand, carClass, weight, driver, engine, speed) {
        super(brand, carClass, weight, driver, engine)
        this.speed = speed

    }
}


const driver = new Driver("Ivan", 5)
const engine = new Engine(200, "BMW");

const car = new Car("BMW X5", "SUV", 2000, driver, engine);
const car1 = new SportCar("BMW X5", "SUV", 2000, driver, engine, 500);
const car2 = new Lorry("BMW X5", "SUV", 2000, driver, engine, 250);

car.start();
car.turnRight();
car.turnLeft();

console.group(1)
console.log(car.toString());
console.groupEnd()

console.group(2)
console.log(car1.toString());
console.groupEnd()

console.group(3)
console.log(car2.toString());
console.groupEnd()