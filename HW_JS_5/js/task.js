class Product {
    constructor(title,image,price,description){
        this.title = title
        this.image = image
        this.price = parseInt(price)
        this.description = description
    }

    createCard (){
        return  `
        <div class="card" style="width: 18rem;">
            <img src="${this.image}" class="card-img-top" alt="${this.title}">
            <div class="card-body">
              <h5 class="card-title">${this.title}</h5>
              <p class="card-text">${this.description}</p>
              <p class="card-text">${this.price} USD</p>
              <a href="#" class="btn btn-primary">Деталі</a>
            </div>
          </div>
        `
    }
}

data.forEach( product => {

    const productObj = new Product(product.title, product.image, product.price, product.description);

    const productCard = productObj.createCard();
   
    document.getElementById("main").innerHTML +=productCard;
});




function filterBy (arr, typeData) {
    if(!Array.isArray(arr) || typeof typeData !== 'string'){
        console.error("Невірно");
        return null
    }

const filArr = arr.filter(el => typeof el !== typeData)

return filArr
}

const filteredArr = filterBy(['hello', 'world', 23, '23', null], 'string');
console.log(filteredArr);