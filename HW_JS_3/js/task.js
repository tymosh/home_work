// 1. Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.

const arr1 = ['a', 'b', 'c']
const arr01 = [1, 2, 3]

const arr02 = arr1.concat(arr01)

document.write("Завдання 1) " + arr02)
document.write("<br>")

// 2. Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.

const arr2 = ['a', 'b', 'c']
arr2.push(1, 2, 3)

document.write("Завдання 2) " + arr2)
document.write("<br>")

// 3. Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].

const arr3 = [1, 2, 3]

document.write("Завдання 3) " + arr3.reverse())
document.write("<br>")

// 4. Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.

const arr4 = [1, 2, 3]
arr4.push(4, 5, 6)

document.write("Завдання 4) " + arr4)
document.write("<br>")

// 5. Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.

const arr5 = [1, 2, 3]
arr5.unshift(4, 5, 6)

document.write("Завдання 5) " + arr5)
document.write("<br>")

// 6. Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.

const arr6 = ['js', 'css', 'jq']

document.write("Завдання 6) " + arr6[0])
document.write("<br>")

// 7. Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].

const arr7 = [1, 2, 3, 4, 5]

document.write("Завдання 7) " + arr7.slice(0, 3))
document.write("<br>")

// 8. Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].

const arr8 = [1, 2, 3, 4, 5]
arr8.splice(1, 2)

document.write("Завдання 8) " + arr8)
document.write("<br>")

// 9. Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].

const arr9 = [1, 2, 3, 4, 5]
arr9.splice(2, 0, 10)

document.write("Завдання 9) " + arr9)
document.write("<br>")

// 10. Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.

const arr10 = [3, 4, 1, 2, 7]

document.write("Завдання 10) " + arr10.sort())
document.write("<br>")

// 11. Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.

const arr11 = ['Привіт, ', 'світ', '!']
arr11.splice(1, 1, "мир")


document.write("Завдання 11) " + arr11[0] + arr11[1] + arr11[2])
document.write("<br>")

// 12. Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').
const arr12 = ['Привіт, ', 'світ', '!']
arr12.splice(0, 1, "Поки,")


document.write("Завдання 12) " + arr12[0] + arr12[1] + arr12[2])
document.write("<br>")

// 13. Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
document.write("Завдання 13) ")
const arr13 = []
arr13.push('1', '2', '3', '4', '5')

for(i=0;i<arr13.length;i++){
    document.write(arr13[i])
}
document.write("<br>")

const arr132 = new Array()
arr132.unshift('1', '2', '3', '4', '5 ')
for(i=0;i<arr132.length;i++){
    document.write(arr132[i])
}
document.write("<br>")
/* 14. Дан багатовимірний масив arr:
            
                var arr = [
                    ['блакитний', 'червоний', 'зелений'],
                    ['blue', 'red', 'green'],
                ];
            
        Виведіть за його допомогою слово 'блакитний' 'blue' .*/

const arr14 = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];

document.write("Завдання 14) " + arr14[0][0] + " " + arr14[1][0])
document.write("<br>")

// 15. Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.

const arr15 = ['a', 'b', 'c', 'd']
document.write("Завдання 15) " + arr15[0] + "+" + arr15[1] + "," + arr15[2] + "+" + arr15[3])
document.write("<br>")


// 16. Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач створіть масив на ту кількість елементів, яку передав користувач. у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.

const arr16number = parseInt(prompt("Число"))
const arr16 = []

document.write("Завдання 16) ")
for (i = 0; i <= arr16number; i++) {
    arr16.push(i)
    document.write(arr16[`${i}`] + ',')
}

document.write("<br>")
// 17. Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.
document.write("Завдання 17) ")
for (i = 0; i < arr16.length; i++) {
    if ((arr16[i] % 2) === 0) {
        document.write(`<span style='color:red; '>${i}</span>`)
    }
    else if ((arr16[i] % 2) != 0) {
        document.write(`<p style='color:green;display:inline-block;'>${i}</p>`)
    }

}
document.write("<br>")
/* 18.Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою.
               
                var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
    
                // Ваш код

                
                document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"
               
           */
document.write("Завдання 18) ")
const vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];

vegetables.splice(3, 1, 'Морквина')

const vegstr = vegetables.join()

document.write(vegstr)
