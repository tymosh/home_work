
function ggg(func1, func2) {
  return func1() + func2();
}


const result = ggg(() => 3, () => 4);
console.log(result);


const str1 = 'var_text_hello';
const arr = str1.split('_');
let result1 = '';
for(let i = 0; i < arr.length; i++){
  result1 += arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
}
console.log(result1);


class MyString {
  constructor(str) {
    this.str = str;
  }

  reverse() {
    return this.str.split('').reverse().join('');
  }

  ucFirst() {
    return this.str.charAt(0).toUpperCase() + this.str.slice(1);
  }

  ucWords() {
    return this.str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
  }
}

// використання класу MyString
const str = new MyString('hello world');
console.log(str.reverse()); // 'dlrow olleh'
console.log(str.ucFirst()); // 'Hello world'
console.log(str.ucWords()); // 'Hello World'




const cryptoWallet = {
  ownerName: "Name Surname",
  bitcoin: {
    currencyName: "Bitcoin",
    logo: "<img src='https://bitcoin.org/img/icons/logotop.svg'>",
    coins: 5,
    currentRate: 10000
  },
  ethereum: {
    currencyName: "Ethereum",
    logo: "<img src = 'https://ethereum.org/static/a5ecd5d3e6236dc2f56e8ea880630812.svg'>",
    coins: 10,
    currentRate: 500
  },
  stellar: {
    currencyName: "Stellar",
    logo: "<img src = 'https://stellar.org/wp-content/themes/stellar/images/favicon/apple-touch-icon.png'>",
    coins: 50,
    currentRate: 0.5
  },
  getCurrencyInfo: function(currency) {
    let info = this[currency];
    let valueInUAH = info.coins * info.currentRate;
    document.write(`Доброго дня, ${this.ownerName}! На вашому балансі ${info.currencyName} (${info.logo}) залишилося ${info.coins} монет, якщо ви сьогодні продасте їх, отримаєте ${valueInUAH} грн.`);
  }
};



let currencyName = prompt("Введіть назву криптовалюти, для якої ви бажаєте отримати інформацію (наприклад, bitcoin, ethereum, stellar):");

cryptoWallet.getCurrencyInfo(currencyName);




function Worker(name, surname, rate, days) {
  this.name = name;
  this.surname = surname;
  this.rate = rate;
  this.days = days;
}

Worker.prototype.getSalary = function () {
  const salary = this.rate * this.days;
  console.log(`${this.name} ${this.surname} отримав ${salary} грн. за ${this.days} днів роботи зі ставкою ${this.rate} грн/день.`);
}


worker1 = new Worker("R", "T", 10, 20)
worker1.getSalary()


