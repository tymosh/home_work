
function Animal(food, location) {
    this.food = food
    this.location = location
}


Animal.prototype.makeNoise = function () {
    return console.log("Тварина спить");
}

Animal.prototype.eat = function () {
    return console.log("Тварина їсть " + this.food);
}

Animal.prototype.sleep = function () {
    return console.log("Тварина спить 2");
}


function Dog(breed,food,location) {
  this.breed = breed;
  this.food = food
    this.location = location
}


Dog.prototype = new Animal()


Dog.prototype.makeNoise = function() {
  console.log("Гав-гав!");
}

Dog.prototype.eat = function() {
  console.log("Собака їсть " + this.food);
}


function Cat( color,food,location) {
  this.color = color;
  this.food = food
    this.location = location
}


Cat.prototype = new Animal();


Cat.prototype.makeNoise = function() {
  console.log("Мяу!");
}

Cat.prototype.eat = function() {
  console.log("Кіт їсть " + this.food);
}


function Horse(speed,food,location) {
  this.speed = speed;
  this.food = food
  this.location = location
}


Horse.prototype = new Animal;

Horse.prototype.makeNoise = function() {
  console.log("Іго-го!");
}

Horse.prototype.eat = function() {
  console.log("Кінь їсть " + this.food);
}


function Vet(){}
  Vet.prototype.treatAnimal = function(animal) {
    console.log(`Тварина їсть ${animal.food} та знаходиться в розташуванні ${animal.location}.`);
  }




function main() {
  const dog = new Dog('Labrador', 'корм', 'дворі');
  const cat = new Cat('сірий', 'корм', 'будинку');
  const horse = new Horse('швидкість', 'овес', 'стайні');
  
  const animals = [dog, cat, horse];
  
  const vet = new Vet();
  
  for (let animal of animals) {
    vet.treatAnimal(animal);
  }
}

main()
