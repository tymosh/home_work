
let number1, number2, operator;

while (isNaN(number1)) {
    number1 = parseInt(prompt("Введіть перше число:"));
}
while (isNaN(number2)) {
    number2 = parseInt(prompt("Введіть друге число:"));
}
while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/") {
    operator = prompt("Введіть математичну операцію (+, -, *, /):");
}


function math(number1, number2, operator) {
    let result
    switch (operator) {
        case "+":
            result = number1 + number2
            break;

        case "-":
            result = number1 - number2
            break;

        case "*":
            result = number1 * number2
            break;
        case "/":
            result = number1 / number2
            break;

        default:
            result = "Невірна операція"
    }

    return result
}

console.log(math(number1, number2, operator))



function checkAge(age) {
    return (age > 18) || confirm('Батьки дозволили?');
} 


function map(fn, array) {
    const result = [];
    for (let i = 0; i < array.length; i++) {
      result.push(fn(array[i]));
    }
    return result;
  }