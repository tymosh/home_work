const pizza = {
    userName: '',
    phone: '',
    email: '',
    size: 85,
    toppings : [],
    sauce : []
}

const price = {
    size: {
        small: 50,
        mid: 80,
        big: 85
    },
    toppings: [
        { price: 20, name: "moc1" },
        { price: 45, name: "moc2" },
        { price: 12, name: "moc3" },
        { price: 93, name: "telya" },
        { price: 78, name: "vetch1" },
        { price: 34, name: "vetch2" },
    ],
    sauce: [
        { price: 60, name: "sauceClassic" },
        { price: 70, name: "sauceBBQ" },
        { price: 50, name: "sauceRikotta" },
    ]
}

window.addEventListener("DOMContentLoaded", () => {
    document.querySelector("form#pizza")
        .addEventListener("click", (e) => {
            //console.log(e.target.id);
            switch (e.target.id) {
                case "small": pizza.size = price.size.small;
                    break;
                case "mid": pizza.size = price.size.mid;
                    break;
                case "big": pizza.size = price.size.big;
                    break;
            }
            show(pizza)
        })
    show(pizza)
    document.querySelector("#banner")
        .addEventListener("mousemove", (e) => {
            randomPositionBanner(e.target, e.clientX, e.clientY)
        })

    document.querySelector("#banner button")
        .addEventListener("click", () => {
            alert("Ваш промокод : XXXXXX")
        })
        show(pizza)
})


function randomPositionBanner(banner) {
    const coords = {
        X: Math.floor(Math.random() * document.body.clientWidth),
        Y: Math.floor(Math.random() * document.body.clientHeight)
    }

    const width = (parseInt(getComputedStyle(document.querySelector("#banner"))["width"]) + 100)

    if (coords.X + width > document.body.clientWidth) {
        return
    }

    if (coords.Y + 100 > document.body.clientWidth) {
        return
    }
    console.log(coords)
    //banner.style.transform = `translateX(300px)`;
    //console.log();
    //document.body.clientWidth / clientHeight

    banner.style.left = coords.X + "px"
    banner.style.top = coords.Y + "px"
}


//валідація 
window.addEventListener('DOMContentLoaded', () => {
    const userName = document.querySelector("[name='name']");
    const userPhone = document.querySelector("[name='phone']");
    const userEmail = document.querySelector("[name='email']");
    const validate = (value, pattern) => pattern.test(value);

    userName.addEventListener('input', () => {
        if (validate(userName.value, /^[а-яіїєґ]{2,}$/i)) {
            userName.classList.add('success');
            userName.classList.remove('error');
            pizza.userName = userName.value;
        }
        else {
            userName.classList.remove('success');
            userName.classList.add('error');
        }
    })


    userPhone.addEventListener('input', () => {
        if (validate(userPhone.value, /^\+380[0-9]{9}$/)) {
            userPhone.classList.add('success');
            userPhone.classList.remove('error');
            pizza.phone = userPhone.value;
        }
        else {
            userPhone.classList.remove('success');
            userPhone.classList.add('error');
        }
    })
    userEmail.addEventListener('change', () => {
        if (validate(userEmail.value, /^[a-z0-9._]{3,40}@[a-z0-9-]{1,777}\.[.a-z]{2,10}$/i)) {
            userEmail.classList.add('success');
            userEmail.classList.remove('error');
            pizza.email = userEmail.value;
        }
        else {
            userEmail.classList.remove('success');
            userEmail.classList.add('error');
        }
    })
    show(pizza)
});

//Відображення складу
function show(pizza) {
    const priceElement = document.querySelector("#price");
    const sauceElement = document.querySelector("#sauce");
    const toppingElement = document.querySelector("#topping");

    // Расчет общей цены
    let totalPrice = pizza.size;

    // Отображение выбранных соусов
    const selectedSauces = pizza.sauce.map(sauceId => {
        const sauce = price.sauce.find(item => item.name === sauceId);
        totalPrice += sauce.price;
        return `<span>${sauce.name}</span>`;
    });
    sauceElement.innerHTML = selectedSauces.join("");

    // Отображение выбранных топпингов
    const selectedToppings = pizza.toppings.map(toppingId => {
        const topping = price.toppings.find(item => item.name === toppingId);
        totalPrice += topping.price;
        return `<span>${topping.name}</span>`;
    });
    toppingElement.innerHTML = selectedToppings.join("");

    // Отображение обновленной цены
    priceElement.innerText = totalPrice;
}

//Перетягування.
window.addEventListener("DOMContentLoaded", () => {
    const ingridients = document.querySelector(".ingridients"),
        table = document.querySelector(".table");

    ingridients.addEventListener("dragstart", (e) => {
        //e.target.classList.add("transfer")
        e.dataTransfer.setData("text", e.target.id);
    });

    table.addEventListener("dragenter", () => {
        table.classList.add("transfer")
    })

    table.addEventListener("dragleave", () => {
        table.classList.remove("transfer")
    })

    table.addEventListener("dragover", (e) => {
        e.preventDefault();
        e.stopPropagation();
    })

    table.addEventListener("drop", (e) => {
        e.preventDefault();

        const id = e.dataTransfer.getData("text")

        const img = document.createElement("img");
        img.src = document.getElementById(id).src;
        table.append(img)
        table.classList.remove("transfer")

        console.log(getTopping(id));
        if (getTopping(id)) {
            pizza.toppings.push(id);
        } else if (getSauce(id)) {
            pizza.sauce.push(id);
        }
    
        show(pizza);

    })
    show(pizza)
})

function getTopping(id) {
    const topping = price.toppings.find((item) => item.name === id);
    return topping !== undefined;
}

function getSauce(id) {
    const sauceElement = document.getElementById(id);
    if (sauceElement && sauceElement.nextElementSibling) {
        return sauceElement.nextElementSibling.textContent.trim();
    }
    return null;
}

