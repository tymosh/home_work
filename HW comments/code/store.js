//store(): Это функция, которая отображает таблицу с данными о продуктах в зависимости от текущего window.location.pathname. Она получает данные из localStorage (если они есть) и вызывает функцию createTableElement() для каждого продукта, чтобы добавить его в таблицу.

export function store() {
    const table = document.querySelector("tbody");
    clearHTMLContent(table)

    if (!localStorage.storeProducts || localStorage.storeProducts === "[]") {
        document.getElementById("box-show").insertAdjacentHTML("beforeend", `<div class="box-none">Поки немає даних ❌</div>`)
        return;
    }
    const products = JSON.parse(localStorage.storeProducts);


    products.forEach((product, index) => {
        createTableElement(index + 1, product, "line", editEvent, deleteEvent, table);
    });
    // Перевірка стореджа
}

//readonly: Это объект, содержащий свойства "id" и "category". Эти свойства указывают на названия свойств продуктов, которые будут отображаться в таблице в режиме только для чтения.

const readonly = {
    id: "id",
    category: "category"
}

//createTableElement(number, product, className, editEvent, deleteEvent, table): Это функция, которая создает элементы таблицы для одного продукта. Она принимает информацию о продукте, а также функции editEvent и deleteEvent, которые будут вызываться при нажатии на кнопки "Редагувати" и "Видалити". Функция добавляет элементы таблицы в указанный table.

function createTableElement(number, product, className, editEvent, deleteEvent, table) {
    const { name, quantity, price, status, description, date, category, url } = product;
    const tr = document.createElement("tr");
    const editButton = document.createElement("button");

    editButton.innerText = "Редагувати";
    editButton.style.cursor = "pointer";
    editButton.addEventListener("click", () => {
        editEvent(product);
    });

    const delButton = document.createElement("button");
    delButton.innerText = "Видалити";
    delButton.style.cursor = "pointer";
    delButton.addEventListener("click", () => {
        deleteEvent(product);
    });

    if (window.location.pathname.includes("store")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("restoran")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("video")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(date),
            newTd(url),
            newTd(editButton),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    table.append(tr);
}

//newTd(props): Это функция, которая создает новый элемент td для таблицы и заполняет его содержимое в зависимости от переданного параметра props.

function newTd(props) {

    if (!props) return;
    const td = document.createElement("td");
    if (props.localName === "button") {
        td.append(props);
    }
    else {
        td.innerText = props;
    }
    return td;
}

//deleteEvent(product): Это функция, которая обрабатывает событие "Видалити" для продукта. Она удаляет продукт из массива всех продуктов, сохраненных в localStorage, и обновляет таблицу, вызывая функцию store().

function deleteEvent(product) {
    const { id } = product;
    const allProducts = JSON.parse(localStorage.storeProducts);

    allProducts.splice(allProducts.findIndex(el => id === el.id), 1)
    //allProducts = allProducts.length === 0? undefined : allProducts
    localStorage.storeProducts = JSON.stringify(allProducts);
    store()
}

//editEvent(product): Это функция, которая обрабатывает событие "Редагувати" для продукта. Она создает модальное окно, в котором отображаются свойства продукта, и позволяет пользователю внести изменения и сохранить их.

function editEvent(product) {
    //name, quantity, price, status, description, date, category, url 
    let { id } = product;
    const allProducts = JSON.parse(localStorage.storeProducts);
    const [oldObj] = allProducts.splice(allProducts.findIndex(el => id === el.id), 1);
    const modal = document.querySelector(".container-modal");
    const close = document.querySelector("#modal-close")

    modal.classList.remove("hide");
    close.addEventListener("click", () => {
        modal.classList.add("hide");
    })
    showPropertyProduct(oldObj, allProducts, modal)
}

//showPropertyProduct(p, arr, modal): Это функция, которая отображает свойства продукта в модальном окне для редактирования. Она создает элементы формы с полями ввода для каждого свойства продукта, позволяя пользователю редактировать значения.

function showPropertyProduct(p, arr, modal) {
    const edit = document.getElementById("edit");
    const props = Object.entries(p);
    const btnSave = document.createElement("button")
    btnSave.type = "button";
    btnSave.innerText = "Зберегти"
    btnSave.addEventListener("click", () => {
        // 1
        const [...inputs] = document.querySelectorAll("#edit input");
        //console.log(inputs);

        const newObjectProduct = {

        }

        inputs.forEach((input) => {
            newObjectProduct[input.key] = input.value
        })

        if(newObjectProduct.quantity > 0){
            newObjectProduct.status = true;
        }else{
            newObjectProduct.status = false;
        }

        arr.push(newObjectProduct);
        localStorage.storeProducts = JSON.stringify(arr);
        modal.classList.add("hide");
        store()
    })

    const formData = props.map(([a1, a2]) => {
        return createPropertyElement(a1, a2)
    })

    clearHTMLContent(edit)
    edit.append(...formData, btnSave)
}

//createPropertyElement(name, value, fnEvent): Это функция, которая создает элементы формы с полями ввода для отображения свойств продукта в модальном окне редактирования. В зависимости от типа свойства, она может сделать поле только для чтения.

function createPropertyElement(name, value, fnEvent) {
    const id = Math.floor(Math.random() * 1000000);
    const div = document.createElement("div");
    const label = document.createElement("label");
    const input = document.createElement("input");
    input.key = name;
    label.setAttribute("for", id);
    input.id = id;
    input.value = value;
    label.innerText = name;

    input.addEventListener("click", () => {
        //fnEvent()
    })

    if (name === readonly.id || name === readonly.category) {
        input.readOnly = true;
        //input.disabled = true
    }

    if (name === "status") {
        return ""
    }

    div.append(label, input);
    return div
}

//clearHTMLContent(el): Это функция, которая очищает содержимое элемента el, удаляя все его дочерние элементы.

function clearHTMLContent(el) {
    if (typeof el !== "object") return;
    el.innerHTML = "";
}
