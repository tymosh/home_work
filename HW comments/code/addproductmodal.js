//showForm: Это функция, которая импортируется из файла functions.js. Однако, само определение функции отсутствует в этом коде, поэтому не могу предоставить подробный комментарий. Она предположительно используется для отображения формы в модальном окне на основе значения, переданного в качестве аргумента.

import { showForm } from "./functions.js";

const modal = document.querySelector(".container-modal");
const modalBody = document.querySelector(".modal-body");
const addProduct = document.querySelector("#btn-add-product");
const modalClose = document.getElementById("modal-close");
const productSelect = document.getElementById("product-select");

//addProduct: Это переменная, которая содержит ссылку на кнопку с идентификатором "btn-add-product". Затем на эту кнопку устанавливается слушатель события "click", который удаляет класс "hide" у элемента с классом "container-modal". Предположительно, это открывает модальное окно, когда пользователь нажимает на кнопку "Добавить продукт".

addProduct.addEventListener("click", () => {
    modal.classList.remove("hide");
})

//modalClose: Это переменная, которая содержит ссылку на элемент с идентификатором "modal-close". Затем на этот элемент устанавливается слушатель события "click", который добавляет класс "hide" обратно к элементу с классом "container-modal". Это предположительно закрывает модальное окно при нажатии на элемент "Закрыть" внутри модального окна.

modalClose.addEventListener("click", () => {
    modal.classList.add("hide")
})

//productSelect: Это переменная, которая содержит ссылку на элемент с идентификатором "product-select". Затем на этот элемент устанавливается слушатель события "change", который вызывает функцию showForm с аргументом, равным значению выбранного элемента списка в нижнем регистре. Это предположительно используется для отображения формы, связанной с выбранным продуктом в выпадающем списке.

productSelect.addEventListener("change", (e) => {
    //{ id, name, qantity, price, status, date, description} - за допомогою ксласу.
    showForm(e.target.value.toLowerCase())
})