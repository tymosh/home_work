//isAuth(): Эта функция проверяет, авторизован ли пользователь, смотря на наличие значения localStorage.isAuth. Если localStorage.isAuth существует (не является пустой строкой, undefined или null), функция ничего не делает (заканчивает свое выполнение). В противном случае, если текущий location.pathname не содержит "authorization", она перенаправляет пользователя на страницу "/authorization".

function isAuth() {
    if (localStorage.isAuth) {
        return
    } else if (!location.pathname.includes("authorization")) {
        location = "/authorization"
    }
}

try {
    console.log((new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase());
    console.log(new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes());

//Auth(): Это функция, которая выполняет процесс авторизации. Она сравнивает значения, введенные в поля ввода логина и пароля (inputLogin и inputPassword), с сгенерированными значениями loginData и passwordData, соответственно. Если значения совпадают, функция устанавливает localStorage.isAuth в true, удаляет классы "error" у полей ввода логина и пароля и перенаправляет пользователя на главную страницу ("/"). В противном случае, она добавляет классы "error" к полям ввода, чтобы показать, что данные не соответствуют ожидаемым.

    function Auth() {
        const loginData = (new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase();
        const passwordData = new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes();

        if (inputLogin.value === loginData && inputPassword.value === passwordData) {
            localStorage.isAuth = true;
            inputLogin.classList.remove("error");
            inputPassword.classList.remove("error");
            location = "/"
        } else {
            inputLogin.classList.add("error")
            inputPassword.classList.add("error")
        }
    }

    const btn = document.querySelector("#btn");
    const inputLogin = document.querySelector("[data-type='login']");
    const inputPassword = document.querySelector("[data-type='password']");

    inputLogin.addEventListener("change", () => {
        btnShow();
    })

    inputPassword.addEventListener("change", () => {
        btnShow();
    })

    btn.addEventListener("click", () => {
        Auth()
    })

    //btnShow(): Это функция, которая управляет доступностью кнопки "btn" на основе введенных значений в поля ввода логина и пароля. Если оба поля не пусты, кнопка становится активной (не заблокированной), в противном случае она остается заблокированной.

    function btnShow() {
        if (inputLogin.value !== "" && inputPassword.value !== "") {
            btn.disabled = false;
        } else {
            btn.disabled = true;
        }
    }

} catch (e) {
  
}


export { isAuth }

/*
Кроме того, представлены следующие переменные:

btn: Это переменная, содержащая ссылку на кнопку с идентификатором "#btn".

inputLogin: Это переменная, содержащая ссылку на элемент ввода для логина, определенный с атрибутом data-type='login'.

inputPassword: Это переменная, содержащая ссылку на элемент ввода для пароля, определенный с атрибутом data-type='password'.

Также в коде используется попытка-отлавливание исключений (try...catch), но в данном случае блок catch оставлен пустым, поэтому он не делает ничего в случае возникновения ошибки.

Из этого кода экспортируется только функция isAuth с помощью export { isAuth }, что позволяет импортировать эту функцию из других файлов при использовании модульной системы JavaScript.
*/